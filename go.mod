module dd-snmp-script-v2

go 1.21.5

require (
	github.com/aws/aws-sdk-go v1.49.14
	github.com/gosnmp/gosnmp v1.37.0
	gopkg.in/yaml.v2 v2.4.0
)

require github.com/jmespath/go-jmespath v0.4.0 // indirect
