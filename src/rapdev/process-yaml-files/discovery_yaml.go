package process_yaml_files

import (
	"dd-snmp-script-v2/src/rapdev/utils"
	y "gopkg.in/yaml.v2"
	"io/ioutil"
)

type DiscoveryYaml struct {
	Subnet      []SubnetConfig `yaml:"snmp_subnet_config"`
	UtilsConfig `yaml:"snmp_utils_config"`
	RapDevToken `yaml:"rapdev"`
	DDData      `yaml:"datadog"`
}

type SubnetConfig struct {
	Subnet     string     `yaml:"subnet"`
	SnmpConfig SnmpConfig `yaml:"snmp"`
}

type SnmpConfig struct {
	UdpPort          int      `yaml:"udp_port"`
	SnmpVersion      int      `yaml:"snmp_version"`
	AuthName         string   `yaml:"auth_name"`
	AuthKey          string   `yaml:"auth_key"`
	PrivateKey       string   `yaml:"private_key"`
	AuthProtocol     string   `yaml:"auth_protocol"`
	PrivProtocol     string   `yaml:"priv_protocol"`
	CommunityString  string   `yaml:"community_string"`
	BlacklistedHosts []string `yaml:"blacklisted_hosts"`
	Tags             []string `yaml:"tags"`
}

type UtilsConfig struct {
	Timeout            int `yaml:"timeout"`
	Retries            int `yaml:"retries"`
	DiscoveryFrequency int `yaml:"discovery_frequency"`
}

type RapDevToken struct {
	Token string `yaml:"token"`
}

type DDData struct {
	Key string `yaml:"key"`
	App string `yaml:"app"`
}

func ReadDiscoveryYaml() DiscoveryYaml {
	data, err := ioutil.ReadFile("discovery.yaml")
	utils.FatalError(err)

	var discoveryConf DiscoveryYaml
	err = y.Unmarshal(data, &discoveryConf)
	utils.FatalError(err)

	return discoveryConf
}
