package subnet

import (
	process_yaml_files "dd-snmp-script-v2/src/rapdev/process-yaml-files"
	"dd-snmp-script-v2/src/rapdev/utils"
	"net"
	"regexp"
)

type IpWithSnmpSettings struct {
	IPs  []string
	Snmp process_yaml_files.SnmpConfig
}

func GetSubnetIps(discoveryYaml process_yaml_files.DiscoveryYaml) (ipWithSnmpSettings []IpWithSnmpSettings) {
	for _, subnet := range discoveryYaml.Subnet {
		match, err := regexp.Match(".+/32", []byte(subnet.Subnet))
		utils.ErrorPrinter(err)

		if err == nil {
			if match {
				ip, _, _ := net.ParseCIDR(subnet.Subnet)
				ipWithSnmpSettings = append(ipWithSnmpSettings, IpWithSnmpSettings{
					IPs:  []string{ip.String()},
					Snmp: subnet.SnmpConfig,
				})
			} else {
				ip, ipNet, _ := net.ParseCIDR(subnet.Subnet)
				var ips []string
				for ip := ip; ipNet.Contains(ip); ipInc(ip) {
					ips = append(ips, ip.String())
				}
				ipWithSnmpSettings = append(ipWithSnmpSettings, IpWithSnmpSettings{
					IPs:  ips,
					Snmp: subnet.SnmpConfig,
				})
			}
		}
	}

	return ipWithSnmpSettings
}

func ipInc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}
