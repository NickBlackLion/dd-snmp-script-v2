package authorization

import (
	"bytes"
	"dd-snmp-script-v2/src/rapdev/utils"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type RapDevResponse struct {
	CreationDate      string `json:"creationdate"`
	Email             string `json:"email"`
	RapDevKey         string `json:"rapdevkey"`
	S3AccessId        string `json:"s3_access_id"`
	S3SecretAccessKey string `json:"s3_secret_access_key"`
	Status            bool   `json:"status"`
}

func AuthorizeInRapDev(token string) (rapDevResponse RapDevResponse) {
	requestBody, err := json.Marshal(map[string]string{"rapdevkey": token})
	utils.FatalError(err)
	response, err := http.Post("https://snmp.rapdev.io/get-tokens", "application/json", bytes.NewBuffer(requestBody))
	utils.FatalError(err)
	defer response.Body.Close()

	responseBody, err := ioutil.ReadAll(response.Body)
	utils.FatalError(err)

	err = json.Unmarshal(responseBody, &rapDevResponse)
	utils.FatalError(err)

	return rapDevResponse
}
