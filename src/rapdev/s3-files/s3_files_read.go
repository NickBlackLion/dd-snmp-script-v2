package s3_files

import (
	"dd-snmp-script-v2/src/rapdev/authorization"
	"dd-snmp-script-v2/src/rapdev/utils"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"strings"
)

type S3File struct {
	ServerType string
	FileName   string
	File       string
}

func GetS3Files(payload authorization.RapDevResponse) (S3Files []S3File) {
	awsCredentials := credentials.NewStaticCredentials(payload.S3AccessId, payload.S3SecretAccessKey, "")
	awsConfig := aws.Config{Region: aws.String("us-east-1"), Credentials: awsCredentials}
	sess, err := session.NewSession(&awsConfig)
	utils.FatalError(err)

	bucket := s3.New(sess)
	bucketName := aws.String("datadog-snmp")

	bucketObjects, err := bucket.ListObjects(&s3.ListObjectsInput{Bucket: bucketName})
	utils.FatalError(err)

	S3Download := s3manager.NewDownloader(sess)

	for _, bucketObject := range bucketObjects.Contents {
		buff := aws.NewWriteAtBuffer([]byte{})
		splitKey := strings.Split(*bucketObject.Key, "/")
		serverType, fileName := splitKey[0], splitKey[1]
		if fileName == "conf.yaml" || fileName == "discovery patterns" {
			_, err := S3Download.Download(buff, &s3.GetObjectInput{
				Bucket: bucketName,
				Key:    bucketObject.Key,
			})
			utils.ErrorPrinter(err)

			if err == nil {
				serverFile := S3File{
					ServerType: serverType,
					FileName:   fileName,
					File:       string(buff.Bytes()),
				}

				S3Files = append(S3Files, serverFile)
			}
		}
	}

	return S3Files
}
