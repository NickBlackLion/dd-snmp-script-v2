package main

import (
	"dd-snmp-script-v2/src/rapdev/authorization"
	process_yaml_files "dd-snmp-script-v2/src/rapdev/process-yaml-files"
	s3_files "dd-snmp-script-v2/src/rapdev/s3-files"
	"dd-snmp-script-v2/src/rapdev/snmp"
	"dd-snmp-script-v2/src/rapdev/subnet"
)

func main() {
	discoveryYaml := process_yaml_files.ReadDiscoveryYaml()
	parsedSunbnets := subnet.GetSubnetIps(discoveryYaml)
	rapDevPayload := authorization.AuthorizeInRapDev(discoveryYaml.Token)
	s3Files := s3_files.GetS3Files(rapDevPayload)
	snmp.TakeServerResponse(parsedSunbnets, s3Files, discoveryYaml.UtilsConfig)
}
