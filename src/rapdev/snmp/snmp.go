package snmp

import (
	process_yaml_files "dd-snmp-script-v2/src/rapdev/process-yaml-files"
	s3_files "dd-snmp-script-v2/src/rapdev/s3-files"
	"dd-snmp-script-v2/src/rapdev/subnet"
	"dd-snmp-script-v2/src/rapdev/utils"
	"fmt"
	"github.com/gosnmp/gosnmp"
	"log"
	"regexp"
	"strings"
	"sync"
	"time"
)

type DiscoveryPattern struct {
	ServerType  string
	OID         string
	Description string
}

func TakeServerResponse(settings []subnet.IpWithSnmpSettings, files []s3_files.S3File, snmpUtils process_yaml_files.UtilsConfig) {
	var wg sync.WaitGroup
	for _, ips := range settings {
		for _, ip := range ips.IPs {
			snmp := &gosnmp.GoSNMP{
				Target:    ip,
				Port:      uint16(ips.Snmp.UdpPort),
				Community: ips.Snmp.CommunityString,
				Version:   gosnmp.Version2c,
				Timeout:   time.Duration(snmpUtils.Timeout) * time.Second,
				Retries:   snmpUtils.Retries,
				MaxOids:   300,
			}

			var discoveryPatterns []DiscoveryPattern
			for _, file := range files {
				if file.FileName == "discovery patterns" {
					oidRows := strings.Split(file.File, "\n")

					for _, oidRow := range oidRows {
						splitOIDRow := strings.Split(oidRow, " ")
						oid, description := "", ""
						if len(splitOIDRow) > 1 {
							oid, description = splitOIDRow[0], splitOIDRow[1]
						} else {
							oid = splitOIDRow[0]
						}

						discoveryPatterns = append(discoveryPatterns, DiscoveryPattern{
							OID:         oid,
							Description: description,
							ServerType:  file.ServerType,
						})
					}

				}
			}

			wg.Add(1)
			go makeSnmpGet(snmp, discoveryPatterns, &wg)
		}
	}

	wg.Wait()
}

func makeSnmpGet(snmp *gosnmp.GoSNMP, discoveryPatterns []DiscoveryPattern, wg *sync.WaitGroup) {
	defer wg.Done()
	err := snmp.Connect()
	utils.ErrorPrinter(err)
	defer snmp.Conn.Close()

	transformedDiscoveryPatterns := make(map[string][]string)
	for _, discoveryPattern := range discoveryPatterns {
		if !strings.HasSuffix(discoveryPattern.OID, "*") {
			transformedDiscoveryPatterns[discoveryPattern.OID] = []string{}
		}
	}

	for _, discoveryPattern := range discoveryPatterns {
		if !strings.HasSuffix(discoveryPattern.OID, "*") {
			description := transformedDiscoveryPatterns[discoveryPattern.OID]
			description = append(description, discoveryPattern.Description)
			transformedDiscoveryPatterns[discoveryPattern.OID] = description
		}
	}

	var oids []string

	for oid := range transformedDiscoveryPatterns {
		oids = append(oids, oid)
	}

	// TODO add logic for adding found servers into list/map/chan
	if err == nil {
		result, _ := snmp.Get(oids)
		if result != nil {
			for _, variable := range result.Variables {
				for _, discoveryPattern := range discoveryPatterns {
					if variable.Value != nil && variable.Type == gosnmp.OctetString {
						if discoveryPattern.Description != "" {
							match, err := regexp.MatchString(discoveryPattern.Description, fmt.Sprintf("%s", variable.Value))
							utils.ErrorPrinter(err)
							if match {
								log.Printf("in %s %s %s %s %s\n", snmp.Target, discoveryPattern.ServerType, discoveryPattern.Description, variable.Name, variable.Value)
							}
						}
					}
				}
			}
		}
	}
}
